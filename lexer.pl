#!/usr/bin/perl
    use 5.14.2;
    use strict;
    use warnings;
    use Data::Dumper;
    use Tocken;
    use XML::Simple;

say "LOADING INPUT FILE";
my $input_file = 'input.txt'; 
open(my $handle , "<", $input_file) || die "Невозможно открыть $input_file. $!";
 
my $text; 
my $test;
while ($text = <$handle>){
$test.=$text;    
}

lex($test);

sub lex{
my @split = devide(shift);

my @tockenList;

foreach my $i (0 .. $#split) {
        if(defined($split[$i])){
        say $i. " $split[$i] ";
        if ($split[$i] =~ /\w{1}[A-Za-z0-9]*/){
                 shift; say  'create token variable'; push @tockenList, Tocken->new(name => 'variable', text => $split[$i]); 
        }       elsif ($split[$i] =~ /\(|\)|\{|\}|\;|\"/){
                shift; say  'create token devider'; push @tockenList, Tocken->new(name => 'devider', text => $split[$i]);
        }       elsif ($split[$i] =~ /\*|\//){
                shift; say  'create token high op'; push @tockenList, Tocken->new(name => 'hop', text => $split[$i]);
        }       elsif ($split[$i] =~ /\=/){
                shift; say  'create token condition'; push @tockenList, Tocken->new(name => 'equal', text => $split[$i]);
        }       elsif ($split[$i] =~ /\;/){
                shift; say  'create token end op'; push @tockenList, Tocken->new(name => 'end', text => $split[$i]);
        }       elsif ($split[$i] =~ /\+|\-/){
                shift; say  'create token low op'; push @tockenList, Tocken->new(name => 'lop', text => $split[$i]);
        }       else { shift; say "not equals templates";  push @tockenList, Tocken->new(type => 'no_matching', text => $split[$i]);}
        }       else { say "undef"}
}    

my $xs      = XML::Simple->new();
say $xs->XMLout(
    \@tockenList,
    XMLDecl  => '<?xml version="1.0"?>',
    RootName => 'tokens',
     OutputFile => 'tockens.xml'
);

say Dumper \@tockenList;
};


sub devide{
#Close your eyes
$text = shift;
my @split = split(/\s+|\n/, $text);

my @newsplit1;
foreach my $t (@split){
push @newsplit1, split(/(\;|\(|\)|\{|\}|\*|\/|\=|\+|\-)/, $t);
}

my @newsplit2;
foreach my $t (@newsplit1){
push @newsplit2, split(/''/, $t);
}

say Dumper \@newsplit2;
return @newsplit2; 

#You can open eyes now
}