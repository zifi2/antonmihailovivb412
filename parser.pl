#!/usr/bin/perl

    use 5.14.2;
    use strict;
    use Data::Dumper;
    use Tocken;
    use XML::Simple;
    use Scalar::Util qw(looks_like_number);

my $input_file = 'tockens.xml'; 
open(my $handle , "<", $input_file) || die "Невозможно открыть $input_file. $!";
my $simple = XML::Simple->new();
my $tockenHash = $simple->XMLin($handle);
#say Dumper $tockenHash;

my $hash;#ссылка на токен
my @stack;#стеки
my @outstack;
my %variable=(); #массив переменных

nextTocken();
my $bStart = 1;#присваиваем ли значение переменной
my $varname;#име переменной, которой присваивается значение
my $varvalue;
my $temp;#ссылка на переменную для просмотра стека
my $temp2;

semanticAction();

sub semanticAction{
    if($bStart){#проверка правильности присваивания переменной
        if(${$hash}{name} eq 'variable'){
            $varname = ${$hash}{text};
            $bStart = 0;
            nextTocken();
            unless(${$hash}{name} eq "equal"){die "WARN! incorrect exp. There is not =";}
        } else {die "incorrect exp. Variable must be at first";}     
    }
    
    unless($bStart){#текущий токен не первый
        if(${$hash}{name} eq 'variable'){
                if(exists($variable{${$hash}{text}})){
                    push @outstack, $variable{${$hash}{text}};#если переменная заведена, то заносим ее значеие
                }  elsif (looks_like_number(${$hash}{text})){
                        push @outstack, ${$hash}{text};#если переменная не заведена, но похожа на число, считаем ее числом
                    } else {die "WARN! There is not variable ${$hash}{text}";}
         }#конец переменным 
         if(${$hash}{name} eq 'devider' and ${$hash}{text} eq '('){
                        push @stack, ${$hash}{text};#открывающую скобку всегда заносим в стек
         }#онец открывающим скобкам 
         if(${$hash}{name} eq 'devider' and ${$hash}{text} eq ')'){
             $temp = pop @stack;
             while($temp ne '('){
                push @outstack, $temp;
                $temp = pop @stack;
             }             
         }#конец закрывающим
         if(${$hash}{name} eq 'hop'){
                       if(scalar @stack){#если стек не пуст
                            $temp = pop @stack;
                            if($temp eq "+" or $temp eq "-" or $temp eq '('){
                                push @stack, $temp;
                                push @stack, ${$hash}{text};#в стеке меньший приоритет, помещаем
                            } else {
                                while($temp eq '*' or $temp eq '/'){
                                   push @outstack, $temp;#извлекаем все символы, пока не будет меньший приоритет
                                   $temp = pop @stack;
                                }
                                push @stack, $temp;
                                push @stack, ${$hash}{text};
                              }
                            #while( scalar @stack) {push pop @stack, @outstack;}#при умножении и делении извлекаем все из стека 
                       } else {push @stack, ${$hash}{text};}#если стек пуст то ложим знак
         }#конец великим
         if(${$hash}{name} eq 'lop'){
                       if(scalar @stack){#если стек не пуст
                            $temp = pop @stack;
                            if($temp eq '('){
                                push @stack, $temp;
                                push @stack, ${$hash}{text};#в стеке меньший приоритет, помещаем
                            } else {
                                while($temp eq '*' or $temp eq '/' or $temp eq '+' or $temp eq '-'){
                                   push @outstack, $temp;#извлекаем все символы, пока не будет меньший приоритет
                                   $temp = pop @stack;
                                }
                                push @stack, $temp;
                                push @stack, ${$hash}{text};
                              }
                            #while( scalar @stack) {push pop @stack, @outstack;}#при умножении и делении извлекаем все из стека 
                       } else {push @stack, ${$hash}{text};}#если стек пуст то ложим знак
         }#конец остальным
         if(${$hash}{name} eq 'devider' and ${$hash}{text} eq ';'){
           if(scalar @stack){
             $temp = pop @stack;
            push @outstack, $temp;   
           }
           $temp = shift @stack;
           push @outstack, $temp;
           $bStart=1; calculation();
         }#конец всему :)
         if(${$hash}{text} eq '$'){say "the end story";die;}
   }#конец не первым токенам     
    if(scalar @{$tockenHash}){
     nextTocken();
     semanticAction();
    }
}  


sub nextTocken{
$hash = shift {$tockenHash};
}

sub calculation{
         
say "calculation";

    while(@outstack){
        $temp = shift @outstack;
        if(looks_like_number($temp)){
                push @stack, $temp;
        } else{
                if($temp eq '*'){
                    $temp = pop @stack;
                    $temp2 = pop @stack;
                    $temp = $temp2 * $temp;
                    push @stack, $temp;
                }
                if($temp eq '/'){
                    $temp = pop @stack;
                    $temp2 = pop @stack;
                    $temp = $temp2 / $temp;
                    push @stack, $temp;
                }
                if($temp eq '+'){
                    $temp = pop @stack;
                    $temp2 = pop @stack;
                    $temp = $temp2 + $temp;
                    push @stack, $temp;
                }
                if($temp eq '-'){
                    $temp = pop @stack;
                    $temp2 = pop @stack;
                    $temp = $temp2 - $temp;
                    push @stack, $temp;
                }
          }

    }

    $varvalue = pop @stack;
    $variable{$varname} = $varvalue;
    say "Answer:". Dumper %variable;
}

1;
